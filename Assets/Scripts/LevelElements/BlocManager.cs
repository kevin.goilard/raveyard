﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlocManager : MonoBehaviour
{
    [SerializeField]
    public int beatsCount = 35;

    [SerializeField]
    private int beatDelayAfter = 0;

    [SerializeField]
    private LevelLink entranceLink;

    [SerializeField]
    private LevelLink exitLink;

    public bool currentlyUsed;

    private bool hasBeenUsed = false;
    private int currentBlocBeat = 0;

    private List<BeatDependantElement> beatDependantElements = new List<BeatDependantElement>();

    public LinkModes GetEntranceMode()
    {
        return entranceLink.LinkMode;
    }

    public bool IsStart()
    {
        return entranceLink.LinkType == LinkTypes.START;
    }

    public LinkModes GetExitMode()
    {
        return exitLink.LinkMode;
    }

    public void SetPosition(Vector2 targetPosition)
    {
        transform.position = targetPosition;
    }

    public Vector2 GetNextPosiiton()
    {
        return exitLink.transform.position;
    }

    public Orientations GetEntranceOrientation()
    {
        return entranceLink.Orientation;
    }

    public Orientations GetExitOrientation()
    {
        return exitLink.Orientation;
    }

    private void Awake()
    {
        if (entranceLink == null)
            entranceLink = GetComponent<LevelLink>();
        if (exitLink == null)
        {
            foreach (LevelLink l in GetComponentsInChildren<LevelLink>())
            {
                if (l.transform != transform)
                    exitLink = l;
                break;
            }
        }
        foreach (BeatDependantElement b in GetComponentsInChildren<BeatDependantElement>())
        {
            beatDependantElements.Add(b);
            b.gameObject.SetActive(false);
            if (b.beatNumberInBlock == 0 && entranceLink.LinkType == LinkTypes.START)
                b.gameObject.SetActive(true);
        }
        RaveManager.Instance.onRaveLevelChanged.AddListener(RecomputeElementsDisplay);
    }

    /*
    // Use this for initialization
    private void Start()
    {
    }

    // Update is called once per frame
    private void Update()
    {
    }*/

    public void StartBloc()
    {
        RaveManager.Instance.onRaveLevelChanged.RemoveListener(RecomputeElementsDisplay);
        TimingManager.Instance.beatPassed.AddListener(ActualizeElementsDurability);
        currentlyUsed = true;
        ActivateAll();
    }

    public void EndBloc()
    {
        TimingManager.Instance.beatPassed.RemoveListener(ActualizeElementsDurability);
        //currentlyUsed = true;
    }

    public void RecomputeElementsDisplay()
    {
        foreach (BeatDependantElement b in beatDependantElements)
        {
            b.ComputeDisplay();
        }
    }

    public void ActivateAll()
    {
        foreach (BeatDependantElement b in beatDependantElements)
        {
            b.Activate();
        }
    }

    public void ActualizeElementsDurability()
    {
        currentBlocBeat++;
        foreach (BeatDependantElement b in beatDependantElements)
        {
            b.ActualizeElement(currentBlocBeat);
        }
        if (currentBlocBeat == beatsCount)
            LevelManager.Instance.ChangedBloc();
    }

    private void OnDestroy()
    {
        //TimingManager.Instance.beatPassed.RemoveListener(ActualizeElementsDurability);
    }
}