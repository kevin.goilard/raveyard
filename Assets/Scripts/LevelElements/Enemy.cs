﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Classe de base des ennemis
/// </summary>
public class Enemy : MovingObject
{
    private bool rightOrUp = false;
    private bool vertical = false;

    protected override void OverridableStart()
    {
        base.OverridableStart();
        if (vertical)

            rbody.velocity = new Vector2((rightOrUp ? 1 : -1) * maxSpeed, 0);
        else
            rbody.velocity = new Vector2(0, (rightOrUp ? 1 : -1) * maxSpeed);
    }

    // Update is called once per frame
    private void Update()
    {
    }

    private void LateUpdate()
    {
        rbody.velocity = new Vector2((rightOrUp ? 1 : -1) * maxSpeed, rbody.velocity.y);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        rightOrUp = !rightOrUp;
        if (!vertical)
            transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, 1);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        //TODO considerer la collision avec le joueur
        rightOrUp = !rightOrUp;
        if (!vertical)
            transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, 1);
    }

    public override void Die()
    {
        StopAllCoroutines();
        Destroy(gameObject);
    }
}