﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum LinkTypes { START, ENTRANCE, EXIT }

public enum LinkModes { TUNNEL, CONTINUE }

public enum Orientations { UP, DOWN, LEFT, RIGHT }

public class LevelLink : MonoBehaviour
{
    [SerializeField]
    private LinkTypes linkType;

    [SerializeField]
    private LinkModes linkMode;

    [SerializeField]
    private Orientations orientation;

    [SerializeField]
    private Color helpColor = Color.green;

    public LinkTypes LinkType
    {
        get
        {
            return linkType;
        }

        private set
        {
            linkType = value;
        }
    }

    public LinkModes LinkMode
    {
        get
        {
            return linkMode;
        }

        private set
        {
            linkMode = value;
        }
    }

    public Orientations Orientation
    {
        get
        {
            return orientation;
        }

        private set
        {
            orientation = value;
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = helpColor;

        Gizmos.DrawSphere(transform.position, 0.2f);
    }
}