﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlocChangedIndicator : MonoBehaviour
{
    // Use this for initialization
    private void Start()
    {
    }

    // Update is called once per frame
    private void Update()
    {
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<PlayerMovement>() != null)
            LevelManager.Instance.ChangedBloc();
    }
}