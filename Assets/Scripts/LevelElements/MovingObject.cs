﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class MovingObject : MonoBehaviour
{
    public Rigidbody2D rbody;

    protected Vector2 movement = Vector2.zero;

    [SerializeField]
    protected float speed = 15;

    [SerializeField]
    protected float maxSpeed = 2;

    protected Animator animator;

    private void Start()
    {
        OverridableStart();
    }

    protected virtual void OverridableStart()
    {
        rbody = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        if (rbody == null)
            Debug.LogError("Attach a rigidbody to enable movement");
    }

    // Update is called once per frame
    private void Update()
    {
        OverridableUpdate();
    }

    private void FixedUpdate()
    {
        if (Mathf.Abs(rbody.velocity.x) > maxSpeed)
        {
            rbody.velocity = new Vector2((rbody.velocity.x / Mathf.Abs(rbody.velocity.x)) * maxSpeed, rbody.velocity.y);
        }
    }

    protected virtual void OverridableUpdate()
    {
    }

    /// <summary>
    /// Ajoute une force à l'objet pour lui dire de se déplacer dans une certaine direction
    /// </summary>
    /// <param name="forceToAdd">Force à ajouter au momemtum</param>
    public void Move(Vector2 forceToAdd)
    {
        if (rbody != null)
        {
            rbody.AddForce(forceToAdd);
        }
    }

    public virtual void Die()
    {
    }
}