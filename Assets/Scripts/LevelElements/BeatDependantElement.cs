﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeatDependantElement : MonoBehaviour
{
    [SerializeField]
    public int beatNumberInBlock = 0;

    [SerializeField]
    private int beatDurability = 5;

    [SerializeField]
    private bool useOnRaveLevel0 = true;

    [SerializeField]
    private bool useOnRaveLevel1 = false;

    [SerializeField]
    private bool useOnRaveLevel2 = false;

    [SerializeField]
    private bool useOnRaveLevel3 = false;

    [SerializeField]
    private bool useOnRaveLevel4 = false;

    [SerializeField]
    private bool useOnRaveLevel5 = false;

    [SerializeField]
    private bool useOnRaveLevel6 = false;

    private int leftDurability;
    private bool used = true;

    private SpriteRenderer renderer;
    private BoxCollider2D collider;

    private void Awake()
    {
        renderer = GetComponent<SpriteRenderer>();
        collider = GetComponent<BoxCollider2D>();
        if (collider != null && renderer != null)
        {
            collider.autoTiling = false;
            collider.size = renderer.size;
        }
    }

    public void ActualizeElement(int currentBeat)
    {
        /*if (currentBeat > beatNumberInBlock && leftDurability > 0)
        {
            //  leftDurability--;
            if (leftDurability <= 0)
                gameObject.SetActive(false);
        }
        else*/
        if (!gameObject.activeSelf && currentBeat == beatNumberInBlock)
        {
            leftDurability = beatDurability;
            gameObject.SetActive(used);
        }
    }

    public void Activate()
    {
        leftDurability = beatDurability;
        gameObject.SetActive(used);
    }

    public void ComputeDisplay()
    {
        switch (RaveManager.Instance.RaveLevel)
        {
            case 0:
                used = useOnRaveLevel0;
                break;

            case 1:
                used = useOnRaveLevel1;
                break;

            case 2:
                used = useOnRaveLevel2;
                break;

            case 3:
                used = useOnRaveLevel3;
                break;

            case 4:
                used = useOnRaveLevel4;
                break;

            case 5:
                used = useOnRaveLevel4;
                break;

            case 6:
                used = useOnRaveLevel6;
                break;

            default:
                break;
        }
    }
}