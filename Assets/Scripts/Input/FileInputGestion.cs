﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class FileInputGestion : MonoBehaviour
{
    [SerializeField]
    private UnityEvent onUp;

    [SerializeField]
    private UnityEvent onDown;

    [SerializeField]
    private UnityEvent onSubmit;

    [SerializeField]
    private UnityEvent onCancel;

    // Update is called once per frame
    private void Update()
    {
        if (InputManager.MoveUp() && !InputManager.oldMoveUp)
            onUp.Invoke();
        if (InputManager.MoveDown() && !InputManager.oldMoveDown)
            onDown.Invoke();
        if (InputManager.Submit() && !InputManager.oldSubmit)
            onSubmit.Invoke();
        if (InputManager.Cancel() && !InputManager.oldCancel)
            onCancel.Invoke();
    }
}