﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : Singleton<InputManager>
{
    public static bool oldMoveLeft = false;
    public static bool oldMoveRight = false;
    public static bool oldMoveDown = false;
    public static bool oldMoveUp = false;
    public static bool oldJump = false;
    public static bool oldDashLeft = false;
    public static bool oldDashRight = false;
    public static bool OldDashDown = false;
    public static bool oldDashUp = false;
    public static bool oldSubmit = false;
    public static bool oldCancel = false;

    private void LateUpdate()
    {
        oldMoveLeft = MoveLeft();
        oldMoveRight = MoveRight();
        oldMoveDown = MoveDown();
        oldMoveUp = MoveUp();
        oldJump = Jump();
        oldDashLeft = DashLeft();
        oldDashRight = DashRight();
        OldDashDown = DashDown();
        oldDashUp = DashUp();
        oldSubmit = Submit();
        oldCancel = Cancel();
    }

    public static bool MoveLeft()
    {
        return Input.GetAxis("Horizontal") < 0;
    }

    public static bool MoveRight()
    {
        return Input.GetAxis("Horizontal") > 0;
    }

    public static bool MoveUp()
    {
        return Input.GetAxis("Vertical") > 0;
    }

    public static bool MoveDown()
    {
        return Input.GetAxis("Vertical") < 0;
    }

    public static bool Jump()
    {
        return Input.GetAxis("Jump") != 0;
    }

    public static bool DashLeft()
    {
        return Input.GetAxis("HorizontalDash") < 0 || (Input.GetAxis("DashButton") != 0 && Input.GetAxis("Horizontal") < 0);
    }

    public static bool DashRight()
    {
        return Input.GetAxis("HorizontalDash") > 0 || (Input.GetAxis("DashButton") != 0 && Input.GetAxis("Horizontal") > 0);
    }

    public static bool DashDown()
    {
        return Input.GetAxis("VerticalDash") < 0 || (Input.GetAxis("DashButton") != 0 && Input.GetAxis("Vertical") < 0);
    }

    public static bool DashUp()
    {
        return Input.GetAxis("VerticalDash") > 0 || (Input.GetAxis("DashButton") != 0 && Input.GetAxis("Vertical") > 0);
    }

    public static bool Submit()
    {
        return Input.GetAxis("Submit") != 0;
    }

    public static bool Cancel()
    {
        return Input.GetAxis("Cancel") != 0;
    }
}