﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class Movement : MonoBehaviour
{
    [SerializeField]
    protected float speed = 5.0f;

    [SerializeField]
    protected float jumpPower = 900.0f;

    [SerializeField]
    protected Animator animator;

    [SerializeField]
    protected Rigidbody2D playerRigidbody;

    private void Start()
    {
        IdleAnimation();
        if (playerRigidbody == null)
            playerRigidbody = GetComponent<Rigidbody2D>();
    }

    virtual protected void MoveLeft()
    {
        RunningAnimation();
        transform.localScale = Vector3.one + 2 * Vector3.left;
        playerRigidbody.velocity = new Vector2(-speed, playerRigidbody.velocity.y);
    }

    virtual protected void MoveRight()
    {
        RunningAnimation();
        transform.localScale = Vector3.one;
        playerRigidbody.velocity = new Vector2(speed, playerRigidbody.velocity.y);
    }

    virtual protected void StopHorizontalMovement()
    {
        IdleAnimation();
        playerRigidbody.velocity = new Vector2(0, playerRigidbody.velocity.y);
    }

    virtual protected void Jump()
    {
        BruitageManager.PlaySound((int)BruitageManager.SoundEffect.JUMP);
        JumpAnimation();
        playerRigidbody.velocity = Vector2.zero;
        playerRigidbody.AddForce(Vector2.up * jumpPower);
    }

    protected void JumpAnimation()
    {
        animator.SetBool("Jumping", true);
        animator.SetBool("Idle", false);
        animator.SetBool("Running", false);
        animator.SetBool("Dashing", false);
    }

    protected void IdleAnimation()
    {
        animator.SetBool("Jumping", false);
        animator.SetBool("Idle", true);
        animator.SetBool("Running", false);
        animator.SetBool("Dashing", false);
        animator.SetBool("VerticalDash", false);
    }

    protected void RunningAnimation()
    {
        animator.SetBool("Jumping", false);
        animator.SetBool("Idle", false);
        animator.SetBool("Running", true);
        animator.SetBool("Dashing", false);
        animator.SetBool("VerticalDash", false);
    }

    protected void DashingAnimation()
    {
        animator.SetBool("Jumping", false);
        animator.SetBool("Idle", false);
        animator.SetBool("Running", false);
        animator.SetBool("Dashing", true);
        animator.SetBool("VerticalDash", false);
    }

    protected void VerticalDashingAnimation()
    {
        animator.SetBool("Jumping", false);
        animator.SetBool("Idle", false);
        animator.SetBool("Running", false);
        animator.SetBool("Dashing", false);
        animator.SetBool("VerticalDash", true);
    }
}