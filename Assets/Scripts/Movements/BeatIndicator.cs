﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BeatIndicator : MonoBehaviour
{
    public enum ENDING_BEAT { PERFECT, SUCCESS, MISSED, ERROR }

    [SerializeField]
    private Image leftImage;

    [SerializeField]
    private Image rightImage;

    private float initialDelay;
    private float delay;

    private Coroutine moveCorout;

    private GameObjectFactory factory;

    public void Init(GameObjectFactory objFactory, float timeDelay)
    {
        //delay = TimingManager.Instance.GapBetweenSources;
        ActualizePositions();
        factory = objFactory;
        initialDelay = timeDelay;
        delay = timeDelay;
        moveCorout = StartCoroutine(MovementCoroutine());
    }

    private void ActualizePositions()
    {
        leftImage.transform.position = Camera.main.ViewportToScreenPoint(Vector3.Lerp(Vector3.right * 0.5f + Vector3.up * 0.1f, Vector3.right * 0.5f * initialDelay / TimingManager.Instance.VisualizedTime + Vector3.right * 0.5f + Vector3.up * 0.1f, delay / TimingManager.Instance.VisualizedTime));
        rightImage.transform.position = Camera.main.ViewportToScreenPoint(Vector3.Lerp(Vector3.right * 0.5f + Vector3.up * 0.1f, Vector3.left * 0.5f * initialDelay / TimingManager.Instance.VisualizedTime + Vector3.right * 0.5f + Vector3.up * 0.1f, delay / TimingManager.Instance.VisualizedTime));
    }

    public void Terminate(ENDING_BEAT endingReason)
    {
        //TODO consider endingReason
        if (moveCorout != null)
            StopCoroutine(moveCorout);
        moveCorout = null;
        leftImage.enabled = false;
        rightImage.enabled = false;
        factory.Stock(gameObject);
    }

    public void FreeTerminate()
    {
        if (moveCorout != null)
            StopCoroutine(moveCorout);
        moveCorout = null;
        factory.Stock(gameObject);
    }

    private IEnumerator MovementCoroutine()
    {
        ActualizePositions();
        leftImage.enabled = true;
        rightImage.enabled = true;
        yield return new WaitForEndOfFrame();
        while (true)
        {
            delay -= Time.deltaTime;
            ActualizePositions();
            yield return new WaitForEndOfFrame();
        }
    }
}