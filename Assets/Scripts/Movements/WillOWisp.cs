﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WillOWisp : MonoBehaviour
{
    [SerializeField]
    private float targetingDuration;

    [SerializeField]
    private WillOWispTarget target;

    [SerializeField]
    private PlayerMovement player;

    private bool playerIsAlive = true;
    private Vector2 nextTarget;

    // Use this for initialization
    private void Start()
    {
    }

    // Update is called once per frame
    private void Update()
    {
        if (playerIsAlive)
            FollowPlayer();
    }

    public void FollowPlayer()
    {
        transform.position = Vector2.Lerp(transform.position, target.transform.position, 0.1f);
    }

    public void PlayerRevived()
    {
        playerIsAlive = true;
    }

    public void PlayerDied()
    {
        playerIsAlive = false;
        nextTarget = LevelManager.Instance.WillOwispTarget() + Vector2.up * 2;
        StartCoroutine(MoveToTarget());
    }

    private IEnumerator MoveToTarget()
    {
        float end = Time.time + targetingDuration;
        while (Time.time < end)
        {
            transform.position = new Vector2(Mathf.SmoothStep(nextTarget.x, transform.position.x, (end - Time.time) / targetingDuration), Mathf.SmoothStep(nextTarget.y, transform.position.y, (end - Time.time) / targetingDuration));
            yield return null;
        }
        end = Time.time + 0.7f;
        while (Time.time < end)
        {
            yield return null;
        }
        player.transform.position = nextTarget;
        player.Undie();
    }
}