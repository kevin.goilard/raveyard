﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerMovement : Movement
{
    [SerializeField]
    private float dashDuration = 0.2f;

    [SerializeField]
    private float dashCoeff = 3.0f;

    [SerializeField]
    private int maxJump = 2;

    private int leftJumps = 2;

    [SerializeField]
    private int maxDash = 2;

    [SerializeField]
    private GameObject horizontalDashFX;

    [SerializeField]
    private GameObject verticalDashFX;

    [SerializeField]
    private WillOWisp willo;

    [SerializeField]
    private UnityEvent onAction;

    private int leftDashs = 2;

    private Coroutine dashCorout;

    private BlocManager reviveBloc;

    public bool isDied = false;

    public BlocManager ReviveBloc
    {
        get
        {
            return reviveBloc;
        }

        set
        {
            reviveBloc = value;
        }
    }

    private enum Directions { LEFT, RIGHT, UP, DOWN };

    public static PlayerMovement Instance;

    private void Start()
    {
        Instance = this;
    }

    private void Update()
    {
        if (!isDied)
        {
            if (dashCorout == null)
            {
                if (InputManager.MoveRight())
                    MoveRight();
                else if (InputManager.MoveLeft())
                    MoveLeft();
                else if (InputManager.oldMoveLeft || InputManager.oldMoveRight)
                    StopHorizontalMovement();

                if (InputManager.Jump() && !InputManager.oldJump)
                    Jump();
                if (leftDashs > 0 && InputManager.DashLeft() && !InputManager.oldDashLeft)
                    dashCorout = StartCoroutine(DashCoroutine(Directions.LEFT));
                else if (leftDashs > 0 && InputManager.DashRight() && !InputManager.oldDashRight)
                    dashCorout = StartCoroutine(DashCoroutine(Directions.RIGHT));
                else if (RaveManager.Instance.RaveLevel >= 2)
                {
                    if (leftDashs > 0 && InputManager.DashUp() && !InputManager.oldDashUp)
                        dashCorout = StartCoroutine(DashCoroutine(Directions.UP));
                    else if (leftDashs > 0 && InputManager.DashDown() && !InputManager.OldDashDown)
                        dashCorout = StartCoroutine(DashCoroutine(Directions.DOWN));
                }
            }
            else if (leftDashs > 0)
            {
                if (leftDashs > 0 && InputManager.DashLeft() && !InputManager.oldDashLeft)
                {
                    StopCoroutine(dashCorout);
                    dashCorout = StartCoroutine(DashCoroutine(Directions.LEFT));
                }
                else if (leftDashs > 0 && InputManager.DashRight() && !InputManager.oldDashRight)
                {
                    StopCoroutine(dashCorout);
                    dashCorout = StartCoroutine(DashCoroutine(Directions.RIGHT));
                }
                else if (RaveManager.Instance.RaveLevel >= 2)
                {
                    if (leftDashs > 0 && InputManager.DashUp() && !InputManager.oldDashUp)
                    {
                        StopCoroutine(dashCorout);
                        dashCorout = StartCoroutine(DashCoroutine(Directions.UP));
                    }
                    else if (leftDashs > 0 && InputManager.DashDown() && !InputManager.OldDashDown)
                    {
                        StopCoroutine(dashCorout);
                        dashCorout = StartCoroutine(DashCoroutine(Directions.DOWN));
                    }
                }
            }
        }
    }

    protected override void Jump()
    {
        if (leftJumps > 0)
        {
            onAction.Invoke();
            base.Jump();
            if (RaveManager.Instance.RaveLevel >= 1)
            {
                if (RaveManager.Instance.RaveLevel < 6)
                    leftJumps--;
            }
            else
                leftJumps = 0;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        //TODO verifier comportement face aux ennemis
        IdleAnimation();
        leftJumps = maxJump;
        leftDashs = maxDash;
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        leftDashs = maxDash;
    }

    public void Die()
    {
        isDied = true;
        playerRigidbody.simulated = false;
        animator.SetBool("Died", true);
        animator.SetBool("Jumping", false);
        animator.SetBool("Idle", false);
        animator.SetBool("Running", false);
        animator.SetBool("Dashing", false);
        StartCoroutine(DelayForWillo());
    }

    private IEnumerator DelayForWillo()
    {
        float end = Time.time + 1;
        while (end < Time.time)
        {
            yield return null;
        }

        willo.PlayerDied();
    }

    public void Undie()
    {
        animator.SetBool("Died", false);
        StartCoroutine(DelayForRevive());
    }

    private IEnumerator DelayForRevive()
    {
        float end = Time.time + 1;
        while (end > Time.time)
        {
            yield return null;
        }

        willo.PlayerRevived();
        while (!reviveBloc.currentlyUsed)
        {
            yield return null;
        }
        /* while (reviveBloc.currentlyUsed)
         {
             yield return null;
         }*/
        end = Time.time + 0.5f;
        while (end > Time.time)
        {
            yield return null;
        }
        GetBackInTheGame();
    }

    public void GetBackInTheGame()
    {
        playerRigidbody.simulated = true;
        isDied = false;
        IdleAnimation();
    }

    private IEnumerator DashCoroutine(Directions dir)
    {
        BruitageManager.PlaySound((int)BruitageManager.SoundEffect.DASH);
        if (dir == Directions.LEFT || dir == Directions.RIGHT)
        {
            transform.localScale = Vector3.one + (dir == Directions.LEFT ? 2 : 0) * Vector3.left;
            DashingAnimation();
        }
        else
        {
            VerticalDashingAnimation();
            horizontalDashFX.transform.localScale = Vector3.one + (dir == Directions.DOWN ? 2 : 0) * Vector3.down;
        }
        onAction.Invoke();

        if (RaveManager.Instance.RaveLevel != 6)
            leftDashs--;
        float endTime = Time.time + dashDuration;
        Vector2 dashSpeed = (Vector2.up * (dir == Directions.UP ? 1 : (dir == Directions.DOWN ? -1 : 0)) + Vector2.left * (dir == Directions.LEFT ? 1 : (dir == Directions.RIGHT ? -1 : 0))) * speed * dashCoeff;
        while (Time.time < endTime / 2)
        {
            playerRigidbody.velocity = dashSpeed;
            yield return null;
        }
        if (dir == Directions.LEFT || dir == Directions.RIGHT)
        {
            horizontalDashFX.SetActive(true);
        }
        else
        {
            horizontalDashFX.SetActive(true);
        }
        while (Time.time < endTime)
        {
            playerRigidbody.velocity = dashSpeed;
            yield return null;
        }
        horizontalDashFX.SetActive(false);
        verticalDashFX.SetActive(false);
        dashCorout = null;
        StopHorizontalMovement();
        yield return null;
    }
}