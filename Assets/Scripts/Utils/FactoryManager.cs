﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Classe gérant les différents factories
/// </summary>
public static class FactoryManager
{
    /// <summary>
    /// GameObject parent de toutes les factories
    /// </summary>
    private static GameObject parent = null;

    /// <summary>
    /// Dictionary contenant les factories et leur string associé
    /// </summary>
    private static Dictionary<string, GameObjectFactory> factories = new Dictionary<string, GameObjectFactory>();

    /// <summary>
    /// Crée une nouvelle factory
    /// </summary>
    /// <param name="f">Nom de la factory</param>
    /// <param name="baseObject">Objet de base de la factory</param>
    /// <returns>booléen indiquant si tout s'est bien passé</returns>
    public static bool createFactory(string f, GameObject baseObject)
    {
        if (parent == null)
        {
            parent = new GameObject();
            parent.name = "Factories";
        }
        try
        {
            if (factories.ContainsKey(f))
            {
                //Debug.Log("Factory " + f.ToString() + " already existing");
                return false;
            }
            factories.Add(f, new GameObjectFactory(baseObject, f.ToString(), parent.transform));
        }
        catch
        {
            return false;
        }
        //Debug.Log("Factory " + f.ToString() + " created.");
        return true;
    }

    /// <summary>
    /// Ajoute un objet à une factory
    /// </summary>
    /// <param name="f">Nom de la factory</param>
    /// <param name="baseObject">Objet à ajouter à la factory</param>
    public static void stockObject(string f, GameObject obj)
    {
        try
        {
            if (factories.ContainsKey(f))
            {
                factories[f].Stock(obj);
            }
            else
            {
                Debug.Log("Factory " + f.ToString() + " not existing.");
            }
        }
        catch
        {
        }
    }

    /// <summary>
    /// Récupère un objet d'une factory existante
    /// </summary>
    /// <param name="f">Nom de la factory a utiliser</param>
    /// <returns>un GameObject provenant de la factory souhaitée</returns>
    public static GameObject getObject(string f)
    {
        try
        {
            if (factories.ContainsKey(f))
            {
                return factories[f].Get();
            }
            else
            {
                Debug.Log("Factory " + f.ToString() + " not existing.");
                return null;
            }
        }
        catch
        {
            return null;
        }
    }

    /// <summary>
    /// Test si la factory f existe, et renvoie un boolean
    /// </summary>
    /// <param name="f"></param>
    /// <returns></returns>
    public static bool existFactory(string f)
    {
        return factories.ContainsKey(f);
    }
}