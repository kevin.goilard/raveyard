﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Classe gérant une factory simple
/// </summary>
public class GameObjectFactory
{
    /// <summary>
    /// Objet de base de la factory
    /// </summary>
    public GameObject obj;

    /// <summary>
    /// Objet parent de la factory
    /// </summary>
    public Transform parent;

    /// <summary>
    /// Queue contenant les différents objets stockés dans la factory
    /// </summary>
    private Queue<GameObject> stockedObjects = new Queue<GameObject>();

    /// <summary>
    /// Constructeur d'une factory
    /// </summary>
    /// <param name="baseObject">Objet de base de la factory</param>
    /// <param name="parentName">Nom que portera le parent de la factory</param>
    /// <param name="gParent">Parent commun à toutes les factories utilisées</param>
    public GameObjectFactory(GameObject baseObject, string parentName, Transform gParent)
    {
        this.obj = baseObject;
        parent = new GameObject(parentName).transform;
        parent.SetParent(gParent);
        parent.localScale = Vector3.one;
    }

    /// <summary>
    /// Méthode de récupération d'un objet dans une factory
    /// </summary>
    /// <returns>un GameObject stocké dans la factory ou un nouvel objet si nécessaire</returns>
    public GameObject Get()
    {
        GameObject o = null;
        while (stockedObjects.Count > 0 && o == null)
            o = stockedObjects.Dequeue();
        if (stockedObjects.Count == 0 && o == null)
        {
            o = GameObject.Instantiate(obj);
            o.transform.SetParent(parent);
            o.transform.localScale = Vector3.one;
        }
        o.SetActive(true);
        return o;
    }

    /// <summary>
    /// Stocke un GameObject dans la factory
    /// </summary>
    /// <param name="toStock">objet à stocker dans la factory</param>
    public void Stock(GameObject toStock)
    {
        toStock.SetActive(false);
        stockedObjects.Enqueue(toStock);
        toStock.transform.SetParent(parent);
    }
}