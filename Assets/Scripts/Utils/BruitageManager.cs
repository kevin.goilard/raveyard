﻿using System.Collections;
using UnityEngine;

/// <summary>
/// Classe gérant toute la musique du jeu, et stockant tous les liens vers les AudioClip
/// </summary>
[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(AudioLowPassFilter))]
public class BruitageManager : Singleton<BruitageManager>
{
    public enum SoundEffect { DASH, SELECTION, PERFECT, HIT, FAIL, JUMP, RAVE_LEVEL_P, RAVE_LEVEL_M, VALIDATE }

    #region Fields

    /****************************** Sound Effects ******************************/

    [Header("Sounds effects")]
    [SerializeField]
    protected AudioClip dash;

    [SerializeField]
    protected AudioClip selection;

    [SerializeField]
    protected AudioClip perfect;

    [SerializeField]
    protected AudioClip hit;

    [SerializeField]
    protected AudioClip fail;

    [SerializeField]
    protected AudioClip jump;

    [SerializeField]
    protected AudioClip raveLevelPlus;

    [SerializeField]
    protected AudioClip raveLevelMoins;

    [SerializeField]
    protected AudioClip validate;

    /// <summary>
    /// Son actuellement demandé dans les dureeIndispo dernières secondes
    /// </summary>
    private int[] countSon;

    /// <summary>
    /// Temps pendant un son est potentiellement indisponible
    /// </summary>
    private float dureeIndispo = 0.1f;

    protected AudioSource source;

    #endregion Fields

    private void Start()
    {
        source = GetComponent<AudioSource>();
        countSon = new int[System.Enum.GetValues(typeof(SoundEffect)).Length];
        foreach (int i in countSon)
            countSon[i] = 0;
        DontDestroyOnLoad(gameObject);
    }

    /// <summary>
    /// Retourne un sound effect pour être joué dans un autre AudioSource
    /// </summary>
    /// <param name="monSon"></param>
    /// <returns></returns>
    public static AudioClip GetSoundEffect(SoundEffect monSon)
    {
        if (Instance != null)
        {
            return Instance.InstanceGetSoundEffect(monSon);
        }
        return null;
    }

    /// <summary>
    /// Redirection de PlaySoundEffect
    /// </summary>
    /// <param name="monSon"></param>
    /// <returns></returns>
    protected AudioClip InstanceGetSoundEffect(SoundEffect monSon)
    {
        switch (monSon)
        {
            case SoundEffect.DASH:
                return dash;

            case SoundEffect.SELECTION:
                return selection;

            case SoundEffect.PERFECT:
                return perfect;

            case SoundEffect.HIT:
                return hit;

            case SoundEffect.FAIL:
                return fail;

            case SoundEffect.JUMP:
                return jump;

            case SoundEffect.RAVE_LEVEL_P:
                return raveLevelPlus;

            case SoundEffect.RAVE_LEVEL_M:
                return raveLevelMoins;

            case SoundEffect.VALIDATE:
                return validate;

            default:
                return null;
        }
    }

    /// <summary>
    /// Si l'objet possède un AudioSource, lui fait jouer le son demandé
    /// </summary>
    /// <param name="son">0 = son de déplacement, 1 = ouverture menu pause, 2 = retour en arrière, 3 = son de sélection, 4 = son de sélection vaisseau</param>
    public static void PlaySound(int son)
    {
        if (Instance != null)
        {
            Instance.InstancePlayUI(son);
        }
    }

    /// <summary>
    /// Redirection de PlayUI
    /// </summary>
    /// <param name="son"></param>
    public void InstancePlayUI(int son)
    {
        if (source != null)
        {
            source.PlayOneShot(InstanceGetSoundEffect((SoundEffect)son));
        }
    }
}