﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour
{
    [SerializeField]
    private PlayerMovement player;

    [SerializeField]
    private WillOWisp willo;

    private void Start()
    {
        DontDestroyOnLoad(gameObject);
        foreach (SongController s in FindObjectsOfType<SongController>())
        {
            if (s != GetComponent<SongController>())
            {
                Destroy(s.gameObject);
            }
        }
    }

    private void Update()
    {
        if (player != null)
        {
            if (!player.isDied)
            {
                float oldZ = transform.position.z;
                transform.position = player.transform.position;
                transform.position += Vector3.forward * oldZ;
            }
            else
            {
                float oldZ = transform.position.z;
                transform.position = willo.transform.position;
                transform.position += Vector3.forward * oldZ;
            }
        }
        else
        {
            player = FindObjectOfType<PlayerMovement>();
            willo = FindObjectOfType<WillOWisp>();
        }
    }
}