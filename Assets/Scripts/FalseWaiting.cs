﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FalseWaiting : MonoBehaviour
{
    [SerializeField]
    private float waitingTime = 2.0f;

    [SerializeField]
    private FlashingText flashing;

    [SerializeField]
    private Text text;

    [SerializeField]
    private Image image;

    [SerializeField]
    private string waitingText = "Preparing your level...";

    [SerializeField]
    private string endText = "Level ready!\nPress any key to play!";

    private Color visible = Color.black;

    private Color invisible = new Color(Color.black.r, Color.black.g, Color.black.b, 0);

    // Use this for initialization
    private void Start()
    {
        text.text = waitingText;
        StartCoroutine(FakeWaiting());
    }

    // Update is called once per frame
    private void Update()
    {
    }

    private IEnumerator FakeWaiting()
    {
        image.color = visible;
        flashing.StartAnimation();
        float end = Time.time + waitingTime;
        while (end > Time.time)
        {
            yield return false;
        }
        flashing.EndAnimation(true);
        text.text = endText;
        while (!Input.anyKey)
        {
            yield return null;
        }
        end = Time.time + 0.5f;
        while (Time.time < end)
        {
            image.color = Color.Lerp(invisible, visible, (end - Time.time) / 0.5f);
            yield return null;
        }
        image.color = invisible;
        image.gameObject.SetActive(false);
        TimingManager.Instance.LaunchMusic();
        UIManager.Instance.Init();
    }
}