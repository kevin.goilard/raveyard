﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : Singleton<LevelManager>
{
    [SerializeField]
    private List<GameObject> blocsList = new List<GameObject>();

    private List<KeyValuePair<GameObject, BlocManager>> startingBlocList = new List<KeyValuePair<GameObject, BlocManager>>();
    private List<KeyValuePair<GameObject, BlocManager>> tunnelEntranceList = new List<KeyValuePair<GameObject, BlocManager>>();
    private List<KeyValuePair<GameObject, BlocManager>> continueEntranceList = new List<KeyValuePair<GameObject, BlocManager>>();

    public List<KeyValuePair<GameObject, BlocManager>> usedBlocs = new List<KeyValuePair<GameObject, BlocManager>>();

    public int currentIndex = 0;

    public void StartLevel()
    {
        foreach (GameObject g in blocsList)
        {
            BlocManager bloc = g.GetComponent<BlocManager>();
            if (bloc != null)
                if (bloc.IsStart())
                    startingBlocList.Add(new KeyValuePair<GameObject, BlocManager>(g, bloc));
                else
                    switch (bloc.GetEntranceMode())
                    {
                        case LinkModes.CONTINUE:
                            continueEntranceList.Add(new KeyValuePair<GameObject, BlocManager>(g, bloc));
                            break;

                        case LinkModes.TUNNEL:
                            tunnelEntranceList.Add(new KeyValuePair<GameObject, BlocManager>(g, bloc));
                            break;

                        default:
                            break;
                    }
        }
        CreateNextBlock();
        CreateNextBlock();
        CreateNextBlock();
        FirstBloc();
    }

    // Use this for initialization
    private void Start()
    {
        StartLevel();
    }

    // Update is called once per frame
    private void Update()
    {
        bool willSurvive = false;
        foreach (KeyValuePair<GameObject, BlocManager> k in usedBlocs)
        {
            if (Vector2.Distance(k.Value.transform.position, PlayerMovement.Instance.transform.position) < 100)
                willSurvive = true;
        }
        if (!willSurvive && !PlayerMovement.Instance.isDied)
            PlayerMovement.Instance.Die();
    }

    public void CreateNextBlock()
    {
        if (usedBlocs.Count > 0)
        {
            if (usedBlocs[usedBlocs.Count - 1].Value.GetExitMode() == LinkModes.TUNNEL)
                CreatetNextTunnel();
            else if (usedBlocs[usedBlocs.Count - 1].Value.GetExitMode() == LinkModes.CONTINUE)
                CreatetNextContinue();
        }
        else
        {
            CreateStartBloc();
        }
    }

    private void CreatetNextTunnel()
    {
        Orientations toFind = usedBlocs[usedBlocs.Count - 1].Value.GetExitOrientation();
        int NextBeatsCount = TimingManager.Instance.BeatInNextSeconds(5.0f);
        KeyValuePair<GameObject, BlocManager> currentSelection = new KeyValuePair<GameObject, BlocManager>();
        int beatsDiff = -1;
        for (int i = 0; i < tunnelEntranceList.Count; i++)
        {
            int tmpDiff = Mathf.Abs(tunnelEntranceList[i].Value.beatsCount - NextBeatsCount);
            if ((beatsDiff == -1 || tmpDiff < beatsDiff) && tunnelEntranceList[i].Value.GetEntranceOrientation() == toFind)
            {
                beatsDiff = tmpDiff;
                currentSelection = tunnelEntranceList[i];
            }
        };
        GameObject tmp = Instantiate(currentSelection.Key);
        tmp.transform.position = usedBlocs[usedBlocs.Count - 1].Value.GetNextPosiiton();
        usedBlocs.Add(new KeyValuePair<GameObject, BlocManager>(tmp, tmp.GetComponent<BlocManager>()));
    }

    private void CreatetNextContinue()
    {
        Orientations toFind = usedBlocs[usedBlocs.Count - 1].Value.GetExitOrientation();
        int NextBeatsCount = TimingManager.Instance.BeatInNextSeconds(5.0f);
        KeyValuePair<GameObject, BlocManager> currentSelection = new KeyValuePair<GameObject, BlocManager>();
        int beatsDiff = -1;
        for (int i = 1; i < continueEntranceList.Count; i++)
        {
            int tmpDiff = Mathf.Abs(continueEntranceList[i].Value.beatsCount - NextBeatsCount);
            if ((beatsDiff == -1 || tmpDiff < beatsDiff) && tunnelEntranceList[i].Value.GetEntranceOrientation() == toFind)
            {
                beatsDiff = tmpDiff;
                currentSelection = continueEntranceList[i];
            }
        }
        GameObject tmp = Instantiate(currentSelection.Key);
        tmp.transform.position = usedBlocs[usedBlocs.Count - 1].Value.GetNextPosiiton();
        usedBlocs.Add(new KeyValuePair<GameObject, BlocManager>(tmp, tmp.GetComponent<BlocManager>()));
    }

    private void CreateStartBloc()
    {
        KeyValuePair<GameObject, BlocManager> currentSelection = startingBlocList[0];
        GameObject tmp = Instantiate(currentSelection.Key);
        tmp.transform.position = Vector3.zero;//usedBlocs[usedBlocs.Count - 1].Value.GetNextPosiiton();
        usedBlocs.Add(new KeyValuePair<GameObject, BlocManager>(tmp, tmp.GetComponent<BlocManager>()));
    }

    public void ChangedBloc()
    {
        usedBlocs[currentIndex].Value.EndBloc();
        if (currentIndex < 2)
            currentIndex++;
        else if (usedBlocs.Count >= 5)
        {
            //Destroy(usedBlocs[0].Key);
            usedBlocs.RemoveAt(0);
        }
        CreateNextBlock();
        usedBlocs[currentIndex].Value.StartBloc();
    }

    public void FirstBloc()
    {
        CreateNextBlock();
        usedBlocs[currentIndex].Value.StartBloc();
    }

    public Vector2 WillOwispTarget()
    {
        PlayerMovement.Instance.ReviveBloc = usedBlocs[Mathf.Max(Mathf.Min(currentIndex, usedBlocs.Count - 2), 0)].Value;
        return usedBlocs[Mathf.Min(currentIndex, usedBlocs.Count - 2)].Value.GetNextPosiiton();
    }
}