﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class RaveManager : Singleton<RaveManager>
{
    private int raveLevel;
    private int raveScore;
    private int failedCount;

    [SerializeField]
    private int perfectScore = 2;

    [SerializeField]
    private int successScore = 1;

    [SerializeField]
    private int missedScore = -1;

    [SerializeField]
    private int errorScore = -2;

    [SerializeField]
    private int level1Cap = 5;

    [SerializeField]
    private int level2Cap = 10;

    [SerializeField]
    private int level3Cap = 25;

    [SerializeField]
    private int level4Cap = 50;

    [SerializeField]
    private int level5Cap = 75;

    [SerializeField]
    private int level6Cap = 100;

    [SerializeField]
    private int raveCap = 100;

    [SerializeField]
    public UnityEvent onRaveScoreChanged;

    [SerializeField]
    public UnityEvent onRaveLevelChanged;

    [SerializeField]
    public UnityEvent failedChanged;

    [SerializeField]
    public UnityEvent failedFull;

    public int MaxFail
    {
        get
        {
            return 1 + RaveLevel;
        }
    }

    public int RaveLevel
    {
        get
        {
            return raveLevel;
        }

        private set
        {
            if (value > raveLevel)

                BruitageManager.PlaySound((int)BruitageManager.SoundEffect.RAVE_LEVEL_P);
            else if (value < raveLevel)

                BruitageManager.PlaySound((int)BruitageManager.SoundEffect.RAVE_LEVEL_M);
            raveLevel = value;
            onRaveLevelChanged.Invoke();
        }
    }

    public int RaveScore
    {
        get
        {
            return raveScore;
        }

        private set
        {
            raveScore = Mathf.Max(0, Mathf.Min(value, raveCap));

            onRaveScoreChanged.Invoke();
            ActualizeRaveLevel();
        }
    }

    public int FailedCount
    {
        get
        {
            return failedCount;
        }

        private set
        {
            failedChanged.Invoke();
            if (value < MaxFail)
                failedCount = Mathf.Max(value, 0);
            else
            {
                failedCount = 0;
                failedFull.Invoke();
            }
        }
    }

    // Use this for initialization
    private void Start()
    {
    }

    // Update is called once per frame
    private void Update()
    {
        //Debug.Log("Rave score: " + RaveScore + "; Rave Level: " + RaveLevel + "; FailedCount: " + failedCount);
    }

    public void ResetRaveLevel()
    {
        RaveScore = 0;
        RaveLevel = 0;
        FailedCount = 0;
    }

    private void ActualizeRaveLevel()
    {
        if (RaveScore >= level6Cap)
            RaveLevel = 6;
        else if (RaveScore > level5Cap)
            RaveLevel = 5;
        else if (RaveScore > level4Cap)
            RaveLevel = 4;
        else if (RaveScore > level3Cap)
            RaveLevel = 3;
        else if (RaveScore > level2Cap)
            RaveLevel = 2;
        else if (RaveScore > level1Cap)
            RaveLevel = 1;
        else
            RaveLevel = 0;
    }

    public void Perfect()
    {
        RaveScore += perfectScore;
        FailedCount--;
    }

    public void Success()
    {
        RaveScore += successScore;
        FailedCount--;
    }

    public void Missed()
    {
        RaveScore += missedScore;
    }

    public void Error()
    {
        RaveScore += errorScore;
        FailedCount++;
    }
}