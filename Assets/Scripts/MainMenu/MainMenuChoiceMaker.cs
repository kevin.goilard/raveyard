﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuChoiceMaker : MonoBehaviour
{
    [SerializeField]
    private List<FlashingText> choices = new List<FlashingText>();

    private int currentSelection = 0;

    public int CurrentSelection
    {
        get
        {
            return currentSelection;
        }

        set
        {
            int oldSelection = currentSelection;
            currentSelection = Mathf.Max(Mathf.Min(choices.Count - 1, value), 0);
            if (oldSelection != currentSelection)
            {
                choices[oldSelection].EndAnimation(true);
                choices[currentSelection].StartAnimation(false);
            }
        }
    }

    // Use this for initialization
    private void Start()
    {
        if (choices.Count == 0)
            foreach (FlashingText f in GetComponentsInChildren<FlashingText>())
                choices.Add(f);
        Disable();
    }

    // Update is called once per frame
    private void Update()
    {
        if (InputManager.MoveDown() && !InputManager.oldMoveDown)
        {
            CurrentSelection++;

            BruitageManager.PlaySound((int)BruitageManager.SoundEffect.SELECTION);
        }
        else if (InputManager.MoveUp() && !InputManager.oldMoveUp)
        {
            CurrentSelection--;

            BruitageManager.PlaySound((int)BruitageManager.SoundEffect.SELECTION);
        }
        else if (InputManager.Submit() && !InputManager.oldSubmit)
        {
            BruitageManager.PlaySound((int)BruitageManager.SoundEffect.VALIDATE);
            if (currentSelection == 1)
                Application.Quit();
            else
                MainMenuManager.Instance.GoToMusicSelection();
        }
    }

    public void Disable()
    {
        foreach (FlashingText f in choices)
            f.EndAnimation(true);
        this.enabled = false;
    }

    public void Enable()
    {
        this.enabled = true;
        if (CurrentSelection != 0)
            CurrentSelection = 0;
        else
            choices[currentSelection].StartAnimation(false);
    }
}