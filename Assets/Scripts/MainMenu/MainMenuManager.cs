﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuManager : Singleton<MainMenuManager>
{
    private enum MainMenuState { FIRST_SCREEN, SECOND_SCREEN, MUSIC_SELECTION }

    [SerializeField]
    private GameObject mainMenu;

    [SerializeField]
    private GameObject musicSelector;

    [SerializeField]
    private GameObject particles;

    [SerializeField]
    private float switchingDuration;

    [SerializeField]
    private float switchingDelay;

    [SerializeField]
    private Title mainScreenTitle;

    [SerializeField]
    private FlashingText mainScreenFlashing;

    [SerializeField]
    private MainMenuChoiceMaker choiceMaker;

    [SerializeField]
    private FileInputGestion fileInput;

    [SerializeField]
    private FileExplorer fileExplorer;

    [SerializeField]
    private Image mask;

    [SerializeField]
    private LoadingScreenManager loadingManager;

    private Vector2 firstScreenPosition;
    private Vector2 secondScreenPosition;

    private Color visible = Color.black;

    private Color invisible = new Color(Color.black.r, Color.black.g, Color.black.b, 0);

    private MainMenuState currentState = MainMenuState.FIRST_SCREEN;

    private Coroutine movementCoroutine;

    private bool enableInteractions = false;

    private MainMenuState CurrentState
    {
        get
        {
            return currentState;
        }

        set
        {
            currentState = value;
        }
    }

    public void EnableInteractions()
    {
        enableInteractions = true;
    }

    private void Start()
    {
        EnableInteractions();
        firstScreenPosition = transform.localPosition;
        secondScreenPosition = firstScreenPosition + Vector2.up * 1080;
    }

    private void Update()
    {
        if (enableInteractions)
            switch (CurrentState)
            {
                case MainMenuState.FIRST_SCREEN:
                    if (Input.anyKeyDown)
                    {
                        GoToSecondScreen();
                    }
                    break;

                case MainMenuState.SECOND_SCREEN:
                    if (InputManager.Cancel() && !InputManager.oldCancel)
                    {
                        GoToFirstScreen();
                    }
                    break;

                case MainMenuState.MUSIC_SELECTION:
                    if (InputManager.Cancel() && !InputManager.oldCancel)
                    {
                        GoBackFromMusicSelection();
                    }
                    break;

                default:
                    break;
            }
    }

    private void GoToFirstScreen()
    {
        if (movementCoroutine == null)
            movementCoroutine = StartCoroutine(SwitchToFirstScreen());
    }

    private void GoToSecondScreen()
    {
        if (movementCoroutine == null)
            movementCoroutine = StartCoroutine(SwitchToSecondScreen());
    }

    public void GoToMusicSelection()
    {
        if (movementCoroutine == null)
            movementCoroutine = StartCoroutine(SwitchToMusicSelection());
    }

    private void GoBackFromMusicSelection()
    {
        if (movementCoroutine == null)
            movementCoroutine = StartCoroutine(SwitchBackFromMusicSelection());
    }

    private IEnumerator SwitchToSecondScreen()
    {
        float endTime = Time.time + switchingDuration;
        while (Time.time < endTime)
        {
            transform.localPosition = Vector2.Lerp(secondScreenPosition, firstScreenPosition, (endTime - Time.time) / switchingDuration);
            yield return null;
        }
        yield return null;
        transform.localPosition = secondScreenPosition;
        mainScreenFlashing.EndAnimation(false);
        CurrentState = MainMenuState.SECOND_SCREEN;
        choiceMaker.Enable();
        movementCoroutine = null;
    }

    private IEnumerator SwitchToFirstScreen()
    {
        float endTime = Time.time + switchingDuration;
        choiceMaker.Disable();
        while (Time.time < endTime)
        {
            transform.localPosition = Vector2.Lerp(firstScreenPosition, secondScreenPosition, (endTime - Time.time) / switchingDuration);
            yield return null;
        }
        yield return null;
        transform.localPosition = firstScreenPosition;
        mainScreenFlashing.StartAnimation();
        CurrentState = MainMenuState.FIRST_SCREEN;
        movementCoroutine = null;
    }

    private IEnumerator SwitchToMusicSelection()
    {
        float endTime;

        choiceMaker.Disable();
        mask.color = invisible;

        endTime = Time.time + switchingDelay;
        while (Time.time < endTime)
        {
            yield return null;
        }
        endTime = Time.time + switchingDuration;
        while (Time.time < endTime)
        {
            mask.color = Color.Lerp(visible, invisible, (endTime - Time.time) / switchingDuration);
            yield return null;
        }
        mask.color = visible;

        musicSelector.SetActive(true);

        endTime = Time.time + switchingDelay;
        while (Time.time < endTime)
        {
            yield return null;
        }
        endTime = Time.time + switchingDuration;
        while (Time.time < endTime)
        {
            mask.color = Color.Lerp(invisible, visible, (endTime - Time.time) / switchingDuration);
            yield return null;
        }
        mask.color = invisible;
        CurrentState = MainMenuState.MUSIC_SELECTION;
        movementCoroutine = null;

        fileInput.enabled = true;
        fileExplorer.SelectItem(0);
    }

    private IEnumerator SwitchBackFromMusicSelection()
    {
        float endTime;

        fileInput.enabled = false;

        mask.color = invisible;

        endTime = Time.time + switchingDelay;
        while (Time.time < endTime)
        {
            yield return null;
        }
        endTime = Time.time + switchingDuration;
        while (Time.time < endTime)
        {
            mask.color = Color.Lerp(visible, invisible, (endTime - Time.time) / switchingDuration);
            yield return null;
        }
        mask.color = visible;

        musicSelector.SetActive(false);

        endTime = Time.time + switchingDelay;
        while (Time.time < endTime)
        {
            yield return null;
        }
        endTime = Time.time + switchingDuration;
        while (Time.time < endTime)
        {
            mask.color = Color.Lerp(invisible, visible, (endTime - Time.time) / switchingDuration);
            yield return null;
        }
        mask.color = invisible;
        choiceMaker.Enable();

        CurrentState = MainMenuState.SECOND_SCREEN;
        movementCoroutine = null;
    }

    public void LoadLevel()
    {
        loadingManager.LoadLevel("LevelScene");
    }
}