﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FlashingText : MonoBehaviour
{
    [SerializeField]
    private Text targetText;

    [SerializeField]
    public float animationDuration = 0.5f;

    [SerializeField]
    public float animationDelay = 0.3f;

    [SerializeField]
    public Color baseColor;

    private Color invisible;
    private Color visible;

    private Coroutine animationCoroutine;

    // Use this for initialization
    private void Awake()
    {
        if (targetText == null)
            targetText = GetComponent<Text>();
        baseColor = targetText.color;

        invisible = new Color(baseColor.r, baseColor.g, baseColor.b, 0);
        visible = new Color(baseColor.r, baseColor.g, baseColor.b, 1);
    }

    public void StartAnimation(bool fromInvisible = true)
    {
        if (targetText != null && animationCoroutine == null)
            if (fromInvisible)
                animationCoroutine = StartCoroutine(AnimationFromInvisible());
            else
                animationCoroutine = StartCoroutine(AnimationFromVisible());
    }

    public void EndAnimation(bool isVisible)
    {
        StopAllCoroutines();
        animationCoroutine = null;
        targetText.color = isVisible ? visible : invisible;
    }

    private IEnumerator AnimationFromInvisible()
    {
        float endTime;

        while (true)
        {
            endTime = Time.time + animationDelay;
            while (Time.time < endTime)
            {
                yield return null;
            }
            endTime = Time.time + animationDuration;
            while (Time.time < endTime)
            {
                targetText.color = Color.Lerp(visible, invisible, (endTime - Time.time) / animationDuration);
                yield return null;
            }
            targetText.color = visible;
            endTime = Time.time + animationDelay;
            while (Time.time < endTime)
            {
                yield return null;
            }
            endTime = Time.time + animationDuration;
            while (Time.time < endTime)
            {
                targetText.color = Color.Lerp(invisible, visible, (endTime - Time.time) / animationDuration);
                yield return null;
            }
            targetText.color = invisible;
        }
    }

    private IEnumerator AnimationFromVisible()
    {
        float endTime;

        while (true)
        {
            endTime = Time.time + animationDelay;
            while (Time.time < endTime)
            {
                yield return null;
            }
            endTime = Time.time + animationDuration;
            while (Time.time < endTime)
            {
                targetText.color = Color.Lerp(invisible, visible, (endTime - Time.time) / animationDuration);
                yield return null;
            }
            targetText.color = invisible;
            endTime = Time.time + animationDelay;
            while (Time.time < endTime)
            {
                yield return null;
            }
            endTime = Time.time + animationDuration;
            while (Time.time < endTime)
            {
                targetText.color = Color.Lerp(visible, invisible, (endTime - Time.time) / animationDuration);
                yield return null;
            }
            targetText.color = visible;
        }
    }
}