﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Title : MonoBehaviour
{
    [SerializeField]
    private Image image;

    [SerializeField]
    public float animationDelay = 0.5f;

    [SerializeField]
    public float animationDuration = 0.5f;

    [SerializeField]
    public FlashingText flashing;

    private Color invisible = new Color(1, 1, 1, 0);
    private Color visible = new Color(1, 1, 1, 1);

    public void Start()
    {
        if (image == null)
            image = GetComponent<Image>();
        if (image != null)
            image.color = invisible;
        StartAnimation();
    }

    public void StartAnimation()
    {
        if (image != null)
            StartCoroutine(Animation());
    }

    // Update is called once per frame
    private void Update()
    {
    }

    private IEnumerator Animation()
    {
        float endTime = Time.time + animationDelay;
        while (Time.time < endTime)
        {
            yield return null;
        }
        endTime = Time.time + animationDuration;
        while (Time.time < endTime)
        {
            image.color = Color.Lerp(visible, invisible, (endTime - Time.time) / animationDuration);
            yield return null;
        }
        image.color = visible; endTime = Time.time + 0.8f;
        while (Time.time < endTime)
        {
            yield return null;
        }
        if (flashing != null)
            flashing.StartAnimation();
        MainMenuManager.Instance.EnableInteractions();
    }
}