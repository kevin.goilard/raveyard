﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : Singleton<UIManager>
{
    [SerializeField]
    private Slider raveScore;

    [SerializeField]
    private Slider failSkull;

    [SerializeField]
    private Slider timeJauge;

    [SerializeField]
    private Talisman firstTalisman;

    [SerializeField]
    private Talisman secondTalisman;

    [SerializeField]
    private Talisman thirdTalisman;

    [SerializeField]
    private Talisman fourthTalisman;

    [SerializeField]
    private Talisman fifthTalisman;

    [SerializeField]
    private Talisman sixthTalisman;

    [SerializeField]
    private LoadingScreenManager loadingScreen;

    private int oldRaveLevel = 0;
    private bool init = false;

    public void Init()
    {
        timeJauge.maxValue = TimingManager.Instance.synchroSource.clip.length;
        timeJauge.value = 0;
        init = true;
        RaveScoreChanged();
        RaveLevelChanged();
    }

    private void Update()
    {
        if (init = true && TimingManager.Instance.synchroSource != null)
        {
            timeJauge.value = TimingManager.Instance.synchroSource.time;
            if (TimingManager.Instance.synchroSource.clip != null && TimingManager.Instance.synchroSource.time >= TimingManager.Instance.synchroSource.clip.length)
            {
                Destroy(Camera.main.gameObject);
                Destroy(ImportAudio.Instance.gameObject);
                Destroy(SongController.Instance.gameObject);
                loadingScreen.LoadLevel("MainMenu");
            }
        }
    }

    public void RaveScoreChanged()
    {
        raveScore.value = RaveManager.Instance.RaveScore;
    }

    public void RaveLevelChanged()
    {
        switch (RaveManager.Instance.RaveLevel)
        {
            case 0:
                if (oldRaveLevel >= 1)
                    firstTalisman.Unburn();
                break;

            case 1:
                if (oldRaveLevel == 0)
                    firstTalisman.Burn();
                else if (oldRaveLevel >= 2)
                    secondTalisman.Unburn();
                break;

            case 2:
                if (oldRaveLevel <= 1)
                    secondTalisman.Burn();
                else if (oldRaveLevel >= 3)
                    thirdTalisman.Unburn();
                break;

            case 3:
                if (oldRaveLevel <= 2)
                    thirdTalisman.Burn();
                else if (oldRaveLevel >= 4)
                    fourthTalisman.Unburn();
                break;

            case 4:
                if (oldRaveLevel <= 3)
                    fourthTalisman.Burn();
                else if (oldRaveLevel >= 5)
                    sixthTalisman.Unburn();
                break;

            case 5:
                if (oldRaveLevel <= 4)
                    fifthTalisman.Burn();
                else if (oldRaveLevel >= 6)
                    sixthTalisman.Unburn();
                break;

            case 6:
                if (oldRaveLevel <= 5)
                    sixthTalisman.Burn(); ;
                break;
        }
        failSkull.maxValue = RaveManager.Instance.MaxFail;
        oldRaveLevel = RaveManager.Instance.RaveLevel;
    }

    public void FailedLevelChanged()
    {
        failSkull.value = RaveManager.Instance.FailedCount;
    }

    public void BurnFirst()
    {
        firstTalisman.Burn();
    }

    public void UnburnFirst()
    {
        firstTalisman.Unburn();
    }

    /*
    private IEnumerator Test()
    {
        float end;
        for (int i = 0; i < 6; i++)
        {
            end = Time.time + 2.0f;
            firstTalisman.Burn();
            while (Time.time < end)
            {
                yield return null;
            }
            firstTalisman.Unburn();
            while (Time.time < end)
            {
                yield return null;
            }
        }
    }*/
}