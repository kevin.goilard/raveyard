﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Talisman : MonoBehaviour
{
    [SerializeField]
    private Animator talismanAnimator;

    [SerializeField]
    private string burnName;

    [SerializeField]
    private string unburnName;

    private void Start()
    {
    }

    public void Burn()
    {
        talismanAnimator.Play(burnName);
        // talismanAnimator.SetTrigger("BurnTrigger");
    }

    public void Unburn()
    {
        talismanAnimator.Play(unburnName);
        //  talismanAnimator.SetTrigger("UnburnTrigger");
    }
}