﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class TextFileExplorer : MonoBehaviour
{
    private Text textExplorer;
    private int firstShowed = 0;

    [SerializeField]
    private Color defaultColor;

    [SerializeField]
    private Color selectedColor;

    [SerializeField]
    private Color fileColor;

    [SerializeField]
    private int numberofItems = 7;

    [SerializeField]
    private string moreItems = "<--->";

    // Use this for initialization
    private void Awake()
    {
        textExplorer = GetComponent<Text>();
    }

    public void SelectItem(int selectedItem)
    {
        if (selectedItem > firstShowed + numberofItems - 1)
            firstShowed++;
        else if (selectedItem < firstShowed)
            firstShowed--;
        ActualizeDisplay(selectedItem);
    }

    public void SelectFolder(int selectedItem)
    {
        firstShowed = 0;
        ActualizeDisplay(selectedItem);
    }

    public void ActualizeDisplay(int selectedItem)
    {
        textExplorer.text = "";
        if (firstShowed > 0)
        {
            textExplorer.text += moreItems;
        }
        textExplorer.text += "\n";
        for (int i = Mathf.Min(firstShowed, selectedItem); i < Mathf.Min(firstShowed + numberofItems, FileExplorer.Instance.Content.Count); i++)
        {
            if (i == FileExplorer.Instance.CurrentIndex)
            {
                textExplorer.text += "<color=\"" + HexConverter(selectedColor) + "\"> " +
                    (File.Exists(FileExplorer.Instance.Content[i].Value) ? "<color=\"" + HexConverter(fileColor) + "\">" + "-> " + "</color>" : "-> ")
                    + FileExplorer.Instance.Content[i].Key.Substring(0,
                    Mathf.Min(FileExplorer.Instance.Content[i].Key.Length, 35)) + (FileExplorer.Instance.Content[i].Key.Length > 35 ? "..." : "") + "</color>\n";
            }
            else
            {
                textExplorer.text += "<color=\"" + HexConverter(defaultColor) + "\"> " +
                    (File.Exists(FileExplorer.Instance.Content[i].Value) ? "<color=\"" + HexConverter(fileColor) + "\">" + "* " + "</color>" : "* ")
                    + FileExplorer.Instance.Content[i].Key.Substring(0,
                    Mathf.Min(FileExplorer.Instance.Content[i].Key.Length, 35)) + (FileExplorer.Instance.Content[i].Key.Length > 35 ? "..." : "") + "</color>\n";
            }
        }
        if (firstShowed + numberofItems < FileExplorer.Instance.Content.Count)
            textExplorer.text += moreItems;
    }

    private static string HexConverter(Color c)
    {
        return string.Format("#{0:X2}{1:X2}{2:X2}", ToByte(c.r), ToByte(c.g), ToByte(c.b));
    }

    private static byte ToByte(float f)
    {
        f = Mathf.Clamp01(f);
        return (byte)(f * 255);
    }
}