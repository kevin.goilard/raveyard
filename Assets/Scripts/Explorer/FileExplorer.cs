﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class FileExplorer : Singleton<FileExplorer>
{
    [Serializable]
    private class IntUnityEvent : UnityEvent<int>
    { }

    [SerializeField]
    private IntUnityEvent selectedValueChanged;

    [SerializeField]
    private IntUnityEvent folderChanged;

    [SerializeField]
    private UnityEvent selectedFileChanged;

    [SerializeField]
    private TextFileExplorer textExplorer;

    [SerializeField]
    private FileInputGestion fileInput;

    private string currentFolder = "";

    private string currentFile = "";

    /// <summary>
    /// Key = name, value = fullname
    /// </summary>
    private List<KeyValuePair<string, string>> content = new List<KeyValuePair<string, string>>();

    private int currentIndex = 0;

    public string CurrentFolder
    {
        get
        {
            return currentFolder;
        }

        private set
        {
            currentFolder = value;
        }
    }

    public List<KeyValuePair<string, string>> Content
    {
        get
        {
            return content;
        }

        private set
        {
            content = value;
        }
    }

    public int CurrentIndex
    {
        get
        {
            return currentIndex;
        }

        private set
        {
            currentIndex = value;
        }
    }

    public string CurrentFile
    {
        get
        {
            return currentFile;
        }

        private set
        {
            currentFile = value;
            selectedFileChanged.Invoke();
        }
    }

    private void Start()
    {
        Init();
    }

    public void Init()
    {
        CurrentFolder = Environment.GetFolderPath(Environment.SpecialFolder.MyMusic);// Directory.GetCurrentDirectory();
        GenerateMusicContentArray();
        textExplorer.ActualizeDisplay(CurrentIndex);
    }

    public void MoveToParent()
    {
        CurrentFolder = Directory.GetParent(CurrentFolder).FullName;
        folderChanged.Invoke(CurrentIndex);
    }

    public string GetParentDirectory()
    {
        try
        {
            return Directory.GetParent(CurrentFolder).FullName;
        }
        catch
        {
            return "";
        }
    }

    public bool MoveTo(string pathToFolder)
    {
        try
        {
            CurrentFolder = pathToFolder;
            CurrentIndex = 0;
            GenerateMusicContentArray();
        }
        catch //(System.Exception e)
        {
            return false;
        }
        folderChanged.Invoke(CurrentIndex);
        return true;
    }

    private void GenerateMusicContentArray()
    {
        Content.Clear();
        if (CurrentFolder != "")
        {
            List<string> directories = GetChildDirectory().ToList();
            List<string> files = Directory.GetFiles(CurrentFolder, "*.*")
                            .Where(s => s.EndsWith(".mp3") || s.EndsWith(".wav")).ToList();
            try
            {
                Content.Add(new KeyValuePair<string, string>("..", GetParentDirectory()));
            }
            catch
            {
            }
            foreach (string s in directories)
            {
                Content.Add(new KeyValuePair<string, string>(GetDirectoryName(s), s));
            }
            foreach (string s in files)
            {
                Content.Add(new KeyValuePair<string, string>(GetFileName(s), s));
            }
        }
        else
        {
            foreach (string s in Directory.GetLogicalDrives())
            {
                Content.Add(new KeyValuePair<string, string>(s, s));
            }
        }
    }

    public void ValidateSelection()
    {
        BruitageManager.PlaySound((int)BruitageManager.SoundEffect.VALIDATE);
        if (!SelectFile(Content[CurrentIndex].Value))
            MoveTo(Content[CurrentIndex].Value);
    }

    public bool SelectFile(string pathToFile)
    {
        if (File.Exists(pathToFile))
        {
            fileInput.enabled = false;
            CurrentFile = pathToFile;
            ImportAudio.Instance.LoadAudio(CurrentFile);
            DontDestroyOnLoad(ImportAudio.Instance);
            MainMenuManager.Instance.LoadLevel();
        }
        else
            return false;
        return true;
    }

    public string[] GetChildDirectory()
    {
        return Directory.GetDirectories(CurrentFolder);
    }

    public string GetDirectoryName(string path)
    {
        if (Directory.Exists(path))
            return Path.GetFileName(path);
        else
            return "";
    }

    public string[] GetAllFiles()
    {
        return Directory.GetFiles(CurrentFolder);
    }

    public string[] GetFiles(string fileType)
    {
        return Directory.GetFiles(CurrentFolder, "*" + fileType);
    }

    public string GetFileName(string path)
    {
        if (File.Exists(path))
            return Path.GetFileName(path);
        else
            return "";
    }

    public void SelectNextItem()
    {
        BruitageManager.PlaySound((int)BruitageManager.SoundEffect.SELECTION);
        if (CurrentIndex + 1 < Content.Count)
        {
            CurrentIndex++;
        }
        selectedValueChanged.Invoke(CurrentIndex);
    }

    public void SelectPreviousItem()
    {
        BruitageManager.PlaySound((int)BruitageManager.SoundEffect.SELECTION);
        if (CurrentIndex - 1 >= 0)
        {
            CurrentIndex--;
        }
        selectedValueChanged.Invoke(CurrentIndex);
    }

    public void SelectItem(int item)
    {
        CurrentIndex = item;
    }
}