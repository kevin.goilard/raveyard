﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImportAudio : Singleton<ImportAudio>
{
    private static AudioClip clip;

    private bool finishedLoading = false;

    public AudioClip Clip
    {
        get
        {
            return clip;
        }

        private set
        {
            clip = value;
        }
    }

    public bool FinishedLoading
    {
        get
        {
            return finishedLoading;
        }

        set
        {
            finishedLoading = value;
        }
    }

    public void LoadAudio(string path)
    {
        DontDestroyOnLoad(this);
        StartCoroutine(LoadAudioCoroutine(path));
    }

    private IEnumerator LoadAudioCoroutine(string path)
    {
        FinishedLoading = false;
        string FullPath = path;

        string pathLowercase = FullPath.ToLower();

        clip = null;
        if (!(pathLowercase.Contains(".wav") ||
                    pathLowercase.Contains(".mp3") ||
                    pathLowercase.Contains(".ogg")))
        {
            yield break;
        }

        FullPath = FullPath.Replace('\\', '/');
        FullPath = "file:///" + FullPath;
        WWW URL = new WWW(FullPath);
        yield return URL;

        if (pathLowercase.Contains(".mp3"))
        {
            Clip = NAudioPlayer.FromMp3Data(URL.bytes);
        }
        else
        {
            Clip = URL.GetAudioClip(false, false);
        }
        FinishedLoading = true;
    }
}