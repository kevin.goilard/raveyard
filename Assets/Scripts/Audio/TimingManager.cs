﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TimingManager : Singleton<TimingManager>
{
    /*[SerializeField]
    private AudioSource mutedSource;*/

    [SerializeField]
    public AudioSource synchroSource;

    [SerializeField]
    private float visualizedTime = 2.0f;

    [SerializeField]
    private float gapBetweenSources;

    [SerializeField]
    private float gapWithSources = 2.0f;

    [SerializeField]
    private float minDiffBetweenBeats = 0.3f;

    [SerializeField]
    private float delayBeforeStart = 3.0f;

    [SerializeField]
    private float perfectFlexibility = 0.05f;

    [SerializeField]
    private float successFlexibility = 0.1f;

    /*[SerializeField]
    private AudioProcessor processor;*/

    [SerializeField]
    private GameObject indicatorGameobject;

    [SerializeField]
    private Vector2 indicatorStartingPos;

    [SerializeField]
    private Vector2 indicatorEndingPos;

    [SerializeField]
    public UnityEvent perfectEvent;

    [SerializeField]
    public UnityEvent successEvent;

    [SerializeField]
    public UnityEvent missedEvent;

    [SerializeField]
    public UnityEvent errorEvent;

    public UnityEvent beatPassed;

    public SpriteRenderer tmp;

    private Coroutine nextBeatsComputingCorout;
    /*
    public void onOnbeatDetected()
    {
        tmp.material.color = new Color(Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f));
        Debug.Log("Beat!!!");
    }

    public void muteAlert()
    {
        Debug.Log("Mute : " + mutedSource.time);
    }

    public void unmuteAlert()
    {
        Debug.Log("Unmute : " + synchroSource.time);
    }
    */
    private GameObjectFactory indicatorFactory;

    private List<float> beatTimerList = new List<float>();
    private int currentIndice = 0;

    private List<KeyValuePair<float, GameObject>> beatsList = new List<KeyValuePair<float, GameObject>>();

    public float GapBetweenSources
    {
        get
        {
            return gapBetweenSources;
        }

        private set
        {
            gapBetweenSources = value;
        }
    }

    public float VisualizedTime
    {
        get
        {
            return visualizedTime;
        }

        private set
        {
            visualizedTime = value;
        }
    }

    // Use this for initialization
    private void Start()
    {
        synchroSource = SongController.Instance.GetComponent<AudioSource>();
        indicatorFactory = new GameObjectFactory(indicatorGameobject, "Indicators", transform);
    }

    // Update is called once per frame
    private void Update()
    {
        if (beatsList.Count > 0)
            if (synchroSource.time > beatsList[0].Key && synchroSource.time - beatsList[0].Key > successFlexibility)
                ConsumeBeat();
        if (SongController.Instance.preProcessedSpectralFluxAnalyzer != null && SongController.Instance.preProcessedSpectralFluxAnalyzer.peakList.Count > 0 && nextBeatsComputingCorout == null)
        {
            nextBeatsComputingCorout = StartCoroutine(SelectNextBeats());
        }
        if (beatTimerList.Count > currentIndice && beatTimerList[currentIndice] > synchroSource.time)
        {
            currentIndice++;
        }
    }

    public void LaunchMusic()
    {
        StartCoroutine(LaunchPlay());
    }

    public void AddBeat(float time, float delay)
    {
        GameObject nextBeatIndicator = indicatorFactory.Get();
        nextBeatIndicator.GetComponent<BeatIndicator>().Init(indicatorFactory, delay);
        beatsList.Add(new KeyValuePair<float, GameObject>(time, nextBeatIndicator));
        beatTimerList.Add(time);
    }

    public void ConsumeBeat()
    {
        if (beatsList.Count > 0)
        {
            BeatIndicator.ENDING_BEAT reason;
            if (synchroSource.time > beatsList[0].Key && synchroSource.time - beatsList[0].Key > successFlexibility)

                reason = BeatIndicator.ENDING_BEAT.MISSED;
            else if (Mathf.Abs(beatsList[0].Key - synchroSource.time) < perfectFlexibility)
                reason = BeatIndicator.ENDING_BEAT.PERFECT;
            else if (Mathf.Abs(beatsList[0].Key - synchroSource.time) < successFlexibility)
                reason = BeatIndicator.ENDING_BEAT.SUCCESS;
            else
                reason = BeatIndicator.ENDING_BEAT.ERROR;

            //        Debug.Log(reason);
            beatsList[0].Value.GetComponent<BeatIndicator>().Terminate(reason);
            switch (reason)
            {
                case BeatIndicator.ENDING_BEAT.PERFECT:
                    perfectEvent.Invoke();

                    BruitageManager.PlaySound((int)BruitageManager.SoundEffect.PERFECT);
                    break;

                case BeatIndicator.ENDING_BEAT.SUCCESS:
                    successEvent.Invoke();

                    BruitageManager.PlaySound((int)BruitageManager.SoundEffect.HIT);
                    break;

                case BeatIndicator.ENDING_BEAT.MISSED:
                    missedEvent.Invoke();
                    break;

                case BeatIndicator.ENDING_BEAT.ERROR:
                    errorEvent.Invoke();
                    BruitageManager.PlaySound((int)BruitageManager.SoundEffect.FAIL);
                    break;

                default:
                    break;
            }
            beatsList.RemoveAt(0);

            beatPassed.Invoke();
        }
    }

    public int BeatInNextSeconds(float duration)
    {
        int c = 0;
        for (int i = 0; i < beatsList.Count; i++)
        {
            if (beatsList[i].Key - synchroSource.time < duration)
                c++;
            else
                break;
        }
        return c;
    }

    private IEnumerator LaunchPlay()
    {
        while (!ImportAudio.Instance.FinishedLoading)
        {
            yield return null;
        }
        //Debug.Log("song loaded");
        synchroSource.clip = ImportAudio.Instance.Clip;
        SongController.Instance.InitSongController();

        //mutedSource.clip = ImportAudio.Instance.Clip;
        //StopAllCoroutines();
        //processor.InitAudioProcessor();
        //mutedSource.Play();
        //synchroSource.PlayDelayed(gapWithSources);
        synchroSource.Play();
    }

    private IEnumerator SelectNextBeats()
    {
        while (SongController.Instance.preProcessedSpectralFluxAnalyzer.peakList.Count > 0 && SongController.Instance.preProcessedSpectralFluxAnalyzer.peakList[0] < synchroSource.time + 5.0f)
        {
            float tmp = SongController.Instance.preProcessedSpectralFluxAnalyzer.peakList[0];
            if (delayBeforeStart < tmp)
                if (beatsList.Count == 0 || tmp - beatsList[beatsList.Count - 1].Key > minDiffBetweenBeats)
                    AddBeat(tmp, tmp - synchroSource.time);//+ (synchroSource.isPlaying ? gapWithSources : 0));
            /*else if (tmp - beatsList[beatsList.Count - 1].Key < 0.01f)
            {
                KeyValuePair<float, GameObject> tmpBeat = beatsList[beatsList.Count - 1];
                beatsList.RemoveAt(beatsList.Count - 1);
                tmpBeat.Value.GetComponent<BeatIndicator>().FreeTerminate();
                tmp = tmp + tmpBeat.Key / 2;
                AddBeat(tmp, tmp - synchroSource.time);
            }*/
            SongController.Instance.preProcessedSpectralFluxAnalyzer.peakList.RemoveAt(0);
            yield return null;
        }
        nextBeatsComputingCorout = null;
    }
}