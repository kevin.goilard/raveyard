﻿using System.Collections.Generic;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

[CustomEditor(typeof(LoadingScreenManager))]
public class LoadingScreenManagerEditor : Editor
{
    private SerializedProperty blackImage;

    private SerializedProperty startWithLoadScreen;
    private SerializedProperty startScreen;
    private SerializedProperty blackFadeAtStart;
    private SerializedProperty fadeAtStart;
    private SerializedProperty startFadeSpeed;
    private SerializedProperty actionBeforeFade;
    private SerializedProperty actionAfterFade;

    private SerializedProperty fadeBeforeEnd;
    private SerializedProperty endFadeSpeed;

    private SerializedProperty defaultLoadingScreen;
    private SerializedProperty fakeOne;

    private ReorderableList loadingScreens;
    private ReorderableList scenes;
    private List<float> heights = new List<float>();
    private List<float> heights1 = new List<float>();

    private int lastIndex;
    private int indexList;
    private Rect rectList;

    private void OnEnable()
    {
        loadingScreens = new ReorderableList(serializedObject, serializedObject.FindProperty("loadingScreens"));

        blackImage = serializedObject.FindProperty("blackImage");

        startWithLoadScreen = serializedObject.FindProperty("startWithLoadScreen");
        startScreen = serializedObject.FindProperty("startScreen");
        blackFadeAtStart = serializedObject.FindProperty("blackFadeAtStart");
        fadeAtStart = serializedObject.FindProperty("fadeAtStart");
        startFadeSpeed = serializedObject.FindProperty("startFadeSpeed");
        actionBeforeFade = serializedObject.FindProperty("actionBeforeFade");
        actionAfterFade = serializedObject.FindProperty("actionAfterFade");

        fadeBeforeEnd = serializedObject.FindProperty("fadeBeforeEnd");
        endFadeSpeed = serializedObject.FindProperty("endFadeSpeed");

        defaultLoadingScreen = serializedObject.FindProperty("defaultLoadingScreen");
        fakeOne = serializedObject.FindProperty("fakeOne");

        loadingScreens.drawHeaderCallback = (Rect rect) =>
        {
            EditorGUI.LabelField(rect, "Loading screens list");
        };
        loadingScreens.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) =>
        {
            try
            {
                heights[index] = isActive ? 60 : 30;
            }
            catch
            {
                heights.Add(isActive ? 60 : 30);
            }

            var item = loadingScreens.serializedProperty.GetArrayElementAtIndex(index);
            string tmpName = "Loading screen number " + index.ToString();

            if (isActive)
            {
                indexList = index;
                EditorGUI.LabelField(new Rect(rect.x, rect.y, rect.width, rect.height), tmpName);

                EditorGUI.LabelField(new Rect(rect.x, rect.y + 30, rect.width, rect.height), "Loading screen : ");
                EditorGUI.PropertyField(new Rect(rect.x + 60 + 40, rect.y + 30, rect.width - 100 - 30, 17), item.FindPropertyRelative("screen"), GUIContent.none);
            }
            else
            {
                EditorGUI.LabelField(new Rect(rect.x, rect.y, rect.width, rect.height), tmpName);
            }
        };
        loadingScreens.elementHeightCallback = (index) =>
        {
            float ret;
            try
            {
                ret = heights[index];
            }
            catch
            {
                heights.Add(30);
                ret = 30;
            }
            return ret;
        };
        loadingScreens.onRemoveCallback = (ReorderableList list) =>
        {
            indexList = -1;
            scenes = null;
            list.serializedProperty.DeleteArrayElementAtIndex(list.index);
        };
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        EditorGUILayout.Space();
        EditorGUILayout.LabelField("Fade Image", EditorStyles.boldLabel);
        EditorGUILayout.PropertyField(blackImage);

        EditorGUILayout.Space();
        EditorGUILayout.LabelField("Start parameters", EditorStyles.boldLabel);
        EditorGUILayout.PropertyField(startWithLoadScreen);
        if (startWithLoadScreen.boolValue)
        {
            EditorGUILayout.PropertyField(startScreen);
            EditorGUILayout.PropertyField(blackFadeAtStart);
            EditorGUILayout.PropertyField(fadeAtStart);
            if (blackFadeAtStart.boolValue)
                EditorGUILayout.PropertyField(startFadeSpeed);

            EditorGUILayout.Space();
            EditorGUILayout.LabelField("Actions parameters", EditorStyles.boldLabel);
            if (blackFadeAtStart.boolValue)
                EditorGUILayout.PropertyField(actionBeforeFade);
            EditorGUILayout.PropertyField(actionAfterFade);
        }
        EditorGUILayout.Space();
        EditorGUILayout.LabelField("End parameters", EditorStyles.boldLabel);
        EditorGUILayout.PropertyField(fadeBeforeEnd);
        if (fadeBeforeEnd.boolValue)
            EditorGUILayout.PropertyField(endFadeSpeed);

        EditorGUILayout.Space();
        EditorGUILayout.LabelField("Defaults parameters", EditorStyles.boldLabel);
        EditorGUILayout.PropertyField(defaultLoadingScreen);
        EditorGUILayout.PropertyField(fakeOne);

        EditorGUILayout.Space();
        EditorGUILayout.LabelField("Loading screens list", EditorStyles.boldLabel);
        loadingScreens.DoLayoutList();
        if (indexList >= 0 && lastIndex != indexList)
        {
            scenes = new ReorderableList(serializedObject, loadingScreens.serializedProperty.GetArrayElementAtIndex(indexList).FindPropertyRelative("scene"));
            scenes.drawHeaderCallback = (Rect rect1) =>
            {
                EditorGUI.LabelField(rect1, "Associated scenes list");
            };
            scenes.drawElementCallback = (Rect rect1, int index1, bool isActive1, bool isFocused1) =>
            {
                try
                {
                    heights1[index1] = isActive1 ? 55 : 30;
                }
                catch
                {
                    heights1.Add(isActive1 ? 55 : 30);
                }
                SerializedProperty sceneName = scenes.serializedProperty.GetArrayElementAtIndex(index1);
                string tmp = sceneName.stringValue;
                if (tmp == "")
                    tmp = "Scene number " + index1;
                if (isActive1)
                {
                    EditorGUI.LabelField(new Rect(rect1.x, rect1.y, rect1.width, rect1.height), tmp);
                    EditorGUI.LabelField(new Rect(rect1.x, rect1.y + 25, rect1.width, rect1.height), "Scene name : ");
                    EditorGUI.PropertyField(new Rect(rect1.x + 100, rect1.y + 25, rect1.width - 125, 20), sceneName, GUIContent.none);
                }
                else
                {
                    EditorGUI.LabelField(new Rect(rect1.x, rect1.y, rect1.width, rect1.height), tmp);
                }
            };
            scenes.elementHeightCallback = (index1) =>
            {
                float ret;
                try
                {
                    ret = heights1[index1];
                }
                catch
                {
                    heights1.Add(30);
                    ret = 30;
                }
                return ret;
            };
        }
        if (scenes != null)
            scenes.DoLayoutList();
        lastIndex = indexList;

        serializedObject.ApplyModifiedProperties();
    }
}