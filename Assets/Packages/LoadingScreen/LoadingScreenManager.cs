﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadingScreenManager : PrefabSingleton<LoadingScreenManager>
{
    /// <summary>
    /// classe pour les différents écrans de chargement
    /// </summary>
    [System.Serializable]
    public class LoadingScreen
    {
        /// <summary>
        /// Nom des scenes qui ont cet écran de chargement.
        /// </summary>
        [SerializeField]
        public List<string> scene = new List<string>();

        /// <summary>
        /// Objet contenant le sprite, et la bare de chargement de l'écran de chargement.
        /// </summary>
        public GameObject screen;
    }

    /// <summary>
    /// Temps à attendre avant de retirer l'écran de chargement.
    /// </summary>
    private static float START_TIME_BEFORE_DISAPPEAR = 0.25f;

    /// <summary>
    /// Image noire pour le fondu au noir.
    /// </summary>
    [SerializeField, Tooltip("Image noire pour le fondu au noir.")]
    private GameObject blackImage;

    /// <summary>
    /// Lancer le visuel d'écran de chargement au début de la scene ?
    /// </summary>
    [SerializeField, Tooltip("Lancer le visuel d'écran de chargement au début de la scene ?")]
    private bool startWithLoadScreen;

    /// <summary>
    /// Objet contenant l'image avec la bare pleine.
    /// </summary>
    [SerializeField, Tooltip("Objet contenant l'image avec la bare pleine.")]
    private GameObject startScreen;

    /// <summary>
    /// Fondu au noir après le chargement ?
    /// </summary>
    [SerializeField, Tooltip("Fondu au noir après le chargement ?")]
    private bool blackFadeAtStart;

    /// <summary>
    /// Fondu transparent après le chargement ?
    /// </summary>
    [SerializeField, Tooltip("Fondu transparent après le chargement ?")]
    private bool fadeAtStart;

    /// <summary>
    /// Temps que dure le fondu au noir avant le chargement.
    /// </summary>
    [SerializeField, Tooltip("Temps que dure le fondu au noir avant le chargement.")]
    private float startFadeSpeed = 0.03f;

    /// <summary>
    /// Actions à déclencher avant le fondu au noir.
    /// </summary>
    [SerializeField, Tooltip("Actions à déclencher avant le fondu au noir.")]
    private UnityEvent actionBeforeFade;

    /// <summary>
    /// Actions à lancer après le fondu au noir ou à la fin du chargement.
    /// </summary>
    [SerializeField, Tooltip("Actions à lancer après le fondu au noir ou à la fin du chargement.")]
    private UnityEvent actionAfterFade;

    /// <summary>
    /// écran de chargement par défaut.
    /// </summary>
    [SerializeField, Tooltip("écran de chargement par défaut.")]
    private GameObject defaultLoadingScreen;

    /// <summary>
    /// Liste des différents écrans de chargement.
    /// </summary>
    [SerializeField, Tooltip("Liste des différents écrans de chargement.")]
    private List<LoadingScreen> loadingScreens = new List<LoadingScreen>();

    [SerializeField]
    private GameObject fakeOne;

    /// <summary>
    /// Fondu au noir avant le chargement ?
    /// </summary>
    [SerializeField, Tooltip("Fondu au noir après le chargement ?")]
    private bool fadeBeforeEnd;

    /// <summary>
    /// Temps que dure le fondu au noir après le chargement.
    /// </summary>
    [SerializeField, Tooltip("Temps que dure le fondu au noir après le chargement.")]
    private float endFadeSpeed = 0.03f;

    /// <summary>
    /// Variable pour retenir tout un tas de détails sur l'opération asynchrone de chargement
    /// </summary>
    private AsyncOperation async = null;

    /// <summary>
    /// Fonction start de Unity
    /// </summary>
    private void Start()
    {
        if (!startWithLoadScreen)
        {
            return;
        }
        startScreen.SetActive(true);
        /*
        if (startScreen.GetComponentInChildren<Slider>() != null)
        {
            startScreen.GetComponentInChildren<Slider>().value = startScreen.GetComponentInChildren<Slider>().maxValue;
        }
        */
        if (blackFadeAtStart)
        {
            if (actionBeforeFade.GetPersistentEventCount() != 0)
            {
                actionBeforeFade.Invoke();
                StartCoroutine(StartBlackFade());
            }
            else
            {
                StartCoroutine(StartBlackFade());
            }
        }
        else if (fadeAtStart)
        {
            if (actionBeforeFade.GetPersistentEventCount() != 0)
            {
                actionBeforeFade.Invoke();
            }
            else
            {
                StartCoroutine(StartFade());
            }
        }
        else
        {
            StartCoroutine(RemoveStartObject());
        }
    }

    /// <summary>
    /// Lance le fondu au noir du démarrage.
    /// </summary>
    public void BeginStartBlackFade()
    {
        StartCoroutine(StartBlackFade());
    }

    /// <summary>
    /// Lance le fondu transparent du démarrage.
    /// </summary>
    public void BeginStartFade()
    {
        StartCoroutine(StartFade());
    }

    /// <summary>
    /// Attend un peu, retire l'écran de chargement et lance les actions de <seealso cref="actionAfterFade"/>
    /// </summary>
    private IEnumerator RemoveStartObject()
    {
        yield return new WaitForSeconds(START_TIME_BEFORE_DISAPPEAR);
        startScreen.SetActive(false);
        actionAfterFade.Invoke();
    }

    /// <summary>
    /// Charge le niveau qui à pour nom <paramref name="scene"/>.
    /// </summary>
    /// <param name="scene">Nom du niveau à charger.</param>
    public void LoadLevel(string scene)
    {
        GameObject screenToUse = defaultLoadingScreen;
        for (int i = 0; i < loadingScreens.Count; i++)
        {
            if (loadingScreens[i].scene.Contains(scene))
            {
                screenToUse = loadingScreens[i].screen;
                break;
            }
        }
        if (fadeBeforeEnd)
        {
            StartCoroutine(EndBlackFade(scene, screenToUse));
        }
        else
        {
            StartCoroutine(StartLoading(scene, screenToUse));
        }
    }

    private IEnumerator UserFakeLoading(GameObject screenToUse)
    {
        FlashingText tmpFlashing = screenToUse.GetComponentInChildren<FlashingText>();
        if (screenToUse.GetComponentInChildren<FlashingText>() != null)
        {
            tmpFlashing.StartAnimation();
        }
        float startTime = Time.time;
        while (startTime + 4 > Time.time)
        {
            yield return null;
        }
        tmpFlashing.EndAnimation(true);
        tmpFlashing.GetComponent<Text>().text = "Level ready !\nPress any key to play";
        while (!Input.anyKeyDown)
        {
            yield return null;
        }
    }

    /// <summary>
    /// Fait le fondu au noir du démarrage.
    /// </summary>
    private IEnumerator StartBlackFade()
    {
        yield return new WaitForSeconds(START_TIME_BEFORE_DISAPPEAR);
        blackImage.SetActive(true);
        Image image = blackImage.GetComponent<Image>();
        image.color = new Color(image.color.r, image.color.g, image.color.b, 0);
        float endTime = Time.time + startFadeSpeed / 2;
        float startTime = Time.time;
        Color color = image.color;
        while (Time.time < endTime)
        {
            color = image.color;
            color.a = (Time.time - startTime) / (endTime - startTime);
            image.color = color;
            yield return null;
        }
        color = image.color;
        color.a = 1;
        image.color = color;
        startScreen.SetActive(false);
        endTime = Time.time + startFadeSpeed / 2;
        startTime = Time.time;
        color = image.color;
        while (Time.time < endTime)
        {
            color = image.color;
            color.a = 1 - (Time.time - startTime) / (endTime - startTime);
            image.color = color;
            yield return null;
        }
        blackImage.SetActive(false);
        actionAfterFade.Invoke();
    }

    /// <summary>
    /// Fait le fondu transparent du démarrage.
    /// </summary>
    private IEnumerator StartFade()
    {
        yield return new WaitForSeconds(START_TIME_BEFORE_DISAPPEAR);
        blackImage.SetActive(false);
        Image[] images = startScreen.GetComponentsInChildren<Image>();
        float endTime = Time.time + startFadeSpeed;
        float startTime = Time.time;
        Color[] initColor = new Color[images.Length];
        for (int i = 0; i < images.Length; i++)
        {
            initColor[i] = images[i].color;
        }
        Color color = new Color();
        while (Time.time < endTime)
        {
            for (int i = 0; i < images.Length; i++)
            {
                color = images[i].color;
                color.a = initColor[i].a - initColor[i].a * (Time.time - startTime) / (endTime - startTime);
                images[i].color = color;
            }
            yield return null;
        }
        for (int i = 0; i < images.Length; i++)
        {
            initColor[i] = images[i].color;
            color.a = 0;
            images[i].color = color;
        }
        startScreen.SetActive(false);
        actionAfterFade.Invoke();
    }

    /// <summary>
    /// Fait le fondu au noir à la fin.
    /// </summary>
    private IEnumerator EndBlackFade(string scene, GameObject loadingScreen)
    {
        blackImage.SetActive(true);
        Image image = blackImage.GetComponent<Image>();
        image.color = new Color(image.color.r, image.color.g, image.color.b, 0);
        float endTime = Time.time + endFadeSpeed / 2;
        float startTime = Time.time;
        Color color = image.color;
        while (Time.time < endTime)
        {
            color = image.color;
            color.a = (Time.time - startTime) / (endTime - startTime);
            image.color = color;
            yield return null;
        }
        color = image.color;
        color.a = 1;
        image.color = color;
        loadingScreen.SetActive(true);
        endTime = Time.time + endFadeSpeed / 2;
        startTime = Time.time;
        color = image.color;
        while (Time.time < endTime)
        {
            color = image.color;
            color.a = 1 - (Time.time - startTime) / (endTime - startTime);
            image.color = color;
            yield return null;
        }
        blackImage.SetActive(false);
        StartCoroutine(StartLoading(scene, loadingScreen));
    }

    /// <summary>
    /// Lance le chargement.
    /// </summary>
    /// <param name="scene">Nom de la scene à changer.</param>
    /// <param name="loadingScreen">écran de chargement à utiliser.</param>
    private IEnumerator StartLoading(string scene, GameObject loadingScreen)
    {
        loadingScreen.SetActive(true);
        async = SceneManager.LoadSceneAsync(scene);
        StartCoroutine(Loading(loadingScreen));
        yield return async;
    }

    /// <summary>
    /// "Update" du chargement.
    /// </summary>
    /// <param name="loadingScreen">écran de chargement à utiliser.</param>
    private IEnumerator Loading(GameObject loadingScreen)
    {
        //Slider slider = loadingScreen.GetComponentInChildren<Slider>();
        while (!async.isDone)
        {
            /*if (slider != null)
            {
                slider.value = async.progress * slider.maxValue;
            }*/
            yield return new WaitForEndOfFrame();
        }
    }
}